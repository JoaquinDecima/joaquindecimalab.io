import '@fortawesome/fontawesome-free/css/all.css'
import './assets/css/variables.css'
import './assets/css/theme.css'
import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App)
app.mount('#app')
