# My CV

This is a public and open source project that seeks to generate an online CV in a simple way. This project was built with Vue.js, but it is not necessary to have knowledge to be able to make your own CV. It is only enough to clone the project and modify the data found in `/src/data/` in the different `.json` files.

For this site it will be necessary to have a CodersRank account because it has an integration with the plugins of said site

## Gat Start

In the following way you can clone the project, install the dependencies and start it locally:

```shell
git clone https://gitlab.com/JoaquinDecima/joaquindecima.gitlab.io.git
cd joaquindecima.gitlab.io
npm i
npm run dev
```

Next we will see how the files to be edited to customize the site are arranged.

## Personalization

All the files and sections that we will see next are found inside `/src/data/`

> ATTENTION: All icons are from fork-awesome however if you want to migrate to another icon system you may have to make adaptations in the code. To facilitate this we use CDN.

### Contact `contact.json`

```json
[
  /*...*/
  {
    "icon": "fa fa-envelope", // Icon
    "data": "joaquin.decima@gmail.com" // Data
  },
  /*...*/
]
```

### Courses `courses.json`

```json
[
  /*...*/
  {
    "date":"Abril 2022", // Date String
    "school": "Coderhouse", // School
    "title":"Programación Backend", // Title
    "data":"Ha realizado y completado con éxito su curso en Coderhouse. La duración fue de 96 horas dictadas a lo largo de 24 semanas, cumpliendo todos los requisitos académicos exigidos.", // Description
    "img":"https://i.postimg.cc/pTHRWWb5/625326ae2901700024609421.png" // Image
  }
  /*...*/
]
```

### Education `education.json`

```json
[
  /*...*/
  {
    "title": "Tec. Programacion Informatica", // Title
    "date": "2015-Actualidad", // Date String
    "school": "Universidad Nacional de Quilmes" // School
  }
  /*...*/
]
```


### Lenguages `lenguajes.json`

```json
[
  /*...*/
  {
    "leng": "Ingles", // Lenguaje Name
    "stars": [
      "fa fa-star",
      "fa fa-star-half-o",
      "fa fa-star-o",
      "fa fa-star-o",
      "fa fa-star-o"
    ],  // Starts in icons
    "description": "Basico" // Description
  }
  /*...*/
]
```

### Personal `personal.json`

```json
{
  "name": "Joaquin Decima",  // Full Name
  "avatar": "/img/profile.webp", // IMG
  "title": "FullStack Developer", // Title
  "contactURL": "https://t.me/patojad", // Contact Default URL
  "about": "Mi objetivo es poder tomar mi trabajo como algo personal, “Hacer lo que amo y amar lo que hago” siento que es importante para un programador poder acompañar el proyecto desde que es una idea hasta que se vuelve un producto utilizado. En lo personal mi desafío es innovar y enfrentarme a nuevos desafíos constantemente sin alejarme del Software Libre que es lo que tanto amo.", // BIO
  "codersrank": "joaquindecima" // CodersRank username to integration
}
```

### Proyects `proyects.json`

```json
[
  /*...*/
  {
    "title": "Vasak Group", // Title
    "icon": "fa fa-briefcase", // Icon
    "description": "Pequeño emprendimiento de soluciones tecnológicas para emprendimientos, pymes y/o grandes empresas. Soluciones a medidas que generan un valor a tu proyecto con un mínimo costo." // Description
  }
  /*...*/
]
```

### Skills `skills.json`

```json
[
  /*...*/
  {
    "name": "Python", // Skill Name
    "range": "Intermedio", // Skill Range (tooltip)
    "percentage": 63, // Skill percentage (number)
    "data": "Conocimientos en Python 3 Intermedios" // Description
  }
  /*...*/
]
```

### Social `social.json`

```json
[
  /*...*/
  {
    "link": "https://gitlab.com/JoaquinDecima/", // Social URL
    "icon": "fa fa-gitlab",  // Social Icon
    "name": "GitLab"  // Social name
  }
  /*...*/
]
```
